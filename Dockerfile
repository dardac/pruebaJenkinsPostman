FROM node:7.8-slim

# app workdir
WORKDIR /app

# install dependecies
RUN npm install -g newman
RUN npm --allow-root install

# build app source code
COPY . ./

# runtime configs
#ENTRYPOINT ["./entrypoint.sh"]

CMD newman run –reporters junit,json pruebaPostmanJenkins.postman_collection.json –e pruebas.postman_environment.json
